from setuptools import setup

setup(
    name="meteo_grpc_server",
    version="0.1",
    description="gRPC system",
    url="https://gitlab.com/OneSingleTopic/meteo_grpc",
    author="OneSingleTopic",
    author_email="maxime.valin@gmail.com",
    license="MIT",
    packages=["meteo_grpc"],
    install_requires=["grpcio-tools", "pandas", "Flask"],
    extras_require={"dev": ["pytest", "black", "mock"]},
    entry_points={
        "console_scripts": ["meteo_grpc_server=meteo_grpc.command_line:serve"],
    },
)
