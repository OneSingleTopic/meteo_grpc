import os

from flask import Flask, render_template, jsonify

import grpc

import meteo_grpc.io.meteo_data as meteo_data


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)

    @app.route("/")
    def main():
        meteo_data_json, col_names = meteo_data.get_data_json()
        return render_template(
            "new_index.html",
            meteo_data_json=meteo_data_json,
            col_names=col_names,
        )

    @app.route("/test")
    def test_stub():
        meteo_data_json = meteo_data.get_data_stub()
        col_names = ["time", "meterusage"]
        return render_template(
            "index.html", meteo_data_json=meteo_data_json, col_names=col_names
        )

    @app.route("/get_data")
    def get_data_from_stub():
        data_from_stub = meteo_data.clean_data(meteo_data.get_data_stub())
        return jsonify(data_from_stub)

    return app
