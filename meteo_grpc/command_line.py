from meteo_grpc.grpc.meteo_grpc_server import MeteoServer


def serve():
    meteo_server = MeteoServer()
    meteo_server.serve()
