import grpc
import pandas
import math

from meteo_grpc.grpc import meteo_grpc_pb2_grpc, meteo_grpc_pb2

from pathlib import Path

TMP_DATA_PATH = Path("resources/meterusage.csv")


def get_data_json(data_path=TMP_DATA_PATH):
    data_df = pandas.read_csv(data_path, sep=",")
    data_json = data_df.to_dict("records")
    col_names = list(data_df.columns)

    return data_json, col_names


def get_data_stub():
    with grpc.insecure_channel("localhost:50051") as channel:
        stub = meteo_grpc_pb2_grpc.MeteoSenderStub(channel)
        return [
            {"time": data.time, "meterusage": data.meterusage}
            for data in stub.SendMeteo(meteo_grpc_pb2.Date(date_str=""))
        ]


def clean_data(meterusage_list):
    return [
        meterusage_dict
        for meterusage_dict in meterusage_list
        if not math.isnan(meterusage_dict["meterusage"])
    ]
