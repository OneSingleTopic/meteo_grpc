
from concurrent import futures
from pathlib import Path
import pandas

import grpc

from meteo_grpc.grpc import meteo_grpc_pb2, meteo_grpc_pb2_grpc

TMP_DATA_PATH = Path("resources/meterusage.csv")

class MeteoSenderServicer(meteo_grpc_pb2_grpc.MeteoSenderServicer):
    def SendMeteo(self, request, context):
        meteo_data = self.prepare_request_answer()
        for meterusage in meteo_data:
            yield meterusage

    def prepare_request_answer(self):
        meterusage_list = []
        for _, item in self.data_df.iterrows():
            meterusage_list.append( 
                meteo_grpc_pb2.MeteoData(
                    time=item.time,
                    meterusage=item.meterusage
                )
            )
        return meterusage_list

    def load_meteo_database(self, data_df):
        self.data_df=data_df
        
class MeteoServer(object):
    def __init__(self, db_path=TMP_DATA_PATH):
        self.data_df =  pandas.read_csv(TMP_DATA_PATH, sep=",")
        
    def serve(self):
        server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
        meteo_sender_service = MeteoSenderServicer()
        meteo_sender_service.load_meteo_database(data_df=self.data_df)
        meteo_grpc_pb2_grpc.add_MeteoSenderServicer_to_server(
            meteo_sender_service, server
        )
        server.add_insecure_port("[::]:50051")
        server.start()
        server.wait_for_termination()


if __name__ == "__main__":
    meteo_server = MeteoServer()
    meteo_server.serve()
