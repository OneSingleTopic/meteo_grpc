import pytest
import os
import json
import meteo_grpc

import mock


def test_client_data_display(client):
    res = client.get("/")
    assert res.status_code == 200
    html = res.get_data(as_text=True)
    assert '<table id="table_meteo_data"' in html


def generate_mocked_data():
    return [{"time": "", "meterusage": 0.0} for _ in range(10)]


@mock.patch("meteo_grpc.io.meteo_data.get_data_stub",)
def test_get_data_from_client(mock_data, client):
    mock_data.return_value = generate_mocked_data()

    res = client.get("/get_data")
    assert res.status_code == 200
    assert res.json is not None
    assert len(res.json) == 10
    for elem in res.json:
        assert type(elem["time"]) == str
        assert type(elem["meterusage"]) == float


@pytest.mark.skipif(
    os.environ.get("METEO_ENV", "DEV") == "DEV",
    reason="No Grpc server available by default",
)
def test_get_data_from_client_real(client):
    res = client.get("/get_data")
    assert res.status_code == 200
    assert res.json is not None
    assert len(res.json) == 2974
    for elem in res.json:
        assert type(elem["time"]) == str
        assert type(elem["meterusage"]) == float
