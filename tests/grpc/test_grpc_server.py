import pytest
from meteo_grpc.grpc.meteo_grpc_server import (
    MeteoServer,
    MeteoSenderServicer,
)
from meteo_grpc.grpc import meteo_grpc_pb2


@pytest.fixture
def meteo_server():
    return MeteoServer()


@pytest.fixture
def meteo_sender_servicer():
    return MeteoSenderServicer()


def test_init_meteo_server(meteo_server):
    assert meteo_server is not None
    assert len(meteo_server.data_df) == 2975


def test_init_meteo_sender_service(meteo_sender_servicer, meteo_server):
    assert meteo_sender_servicer is not None
    meteo_sender_servicer.load_meteo_database(meteo_server.data_df)
    assert len(meteo_sender_servicer.data_df) == 2975

    prepared_answer = meteo_sender_servicer.prepare_request_answer()
    assert len(prepared_answer) == 2975
    for elem in prepared_answer:
        assert type(elem) == meteo_grpc_pb2.MeteoData
