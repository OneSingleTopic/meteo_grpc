import os
import pytest
import math
from datetime import datetime
from meteo_grpc.io import meteo_data


def test_get_data():
    data_json, col_names = meteo_data.get_data_json()
    assert len(data_json) == 2975
    for elem in data_json:
        assert type(elem) == dict
        assert "time" in elem
        assert type(elem["time"]) == str
        assert (
            type(datetime.strptime(elem["time"], "%Y-%m-%d %H:%M:%S"))
            == datetime
        )
        assert "meterusage" in elem
        assert type(elem["meterusage"]) == float

    assert col_names == ["time", "meterusage"]


@pytest.mark.skipif(
    os.environ.get("METEO_ENV", "DEV") == "DEV",
    reason="No Grpc server available by default",
)
def test_get_data_stub():
    data_json = meteo_data.get_data_stub()
    assert len(data_json) == 2975
    for elem in data_json:
        assert type(elem) == dict
        assert "time" in elem
        assert type(elem["time"]) == str
        assert (
            type(datetime.strptime(elem["time"], "%Y-%m-%d %H:%M:%S"))
            == datetime
        )
        assert "meterusage" in elem
        assert type(elem["meterusage"]) == float


def test_clean_meteo_data():
    meterusage_list = [{"time": "", "meterusage": 1.0} for _ in range(10)]
    meterusage_list[3]["meterusage"] = float("nan")

    processed_meterusage_list = meteo_data.clean_data(meterusage_list)
    assert len(processed_meterusage_list) == 9
    assert all(
        not math.isnan(meterusage["meterusage"])
        for meterusage in processed_meterusage_list
    )
